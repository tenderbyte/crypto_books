defmodule CryptoBooksWeb.PageController do
  use CryptoBooksWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
